import 'package:design_1/home.dart';
import 'package:design_1/profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'hello',
      home: Container(
        child: Home(),
      ),
      routes: {
        Profile.id: (context) => Profile(),
      },
    );
  }
}
