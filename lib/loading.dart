import 'package:design_1/home.dart';
import 'package:design_1/profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoadingScreen extends StatefulWidget {
  static const id = '/';
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://media.istockphoto.com/photos/abstract-blue-background-picture-id1000272468?k=6&m=1000272468&s=612x612&w=0&h=qyFQCYGNCIqAdchuV_qGq0YNdOw48jZV61khsoMV64k="),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 1,
              child: Center(
                child: Text(
                  'boniad',
                  style: GoogleFonts.romanesco(
                      fontSize: 90,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      letterSpacing: 10,
                      fontStyle: FontStyle.italic),
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 300,
                    child: Column(
                      children: [
                        CupertinoTextFormFieldRow(
                          placeholder: 'Enter email',
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.emailAddress,
                          placeholderStyle: TextStyle(color: Colors.white),
                          autocorrect: true,
                          decoration: const BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                width: .4,
                                color: Color(0xFF000000),
                              ),
                            ),
                          ),
                        ),
                        Padding(padding: const EdgeInsets.only(bottom: 8.0)),
                        CupertinoTextFormFieldRow(
                          placeholder: 'Enter password',
                          textAlign: TextAlign.center,
                          placeholderStyle: TextStyle(color: Colors.white),
                          decoration: const BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                width: .4,
                                color: Color(0xFF000000),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.grey.shade400,
                      onSurface: Colors.grey.shade100,
                      minimumSize: Size(150, 20),
                      alignment: Alignment.center,
                    ),
                    child: Icon(Icons.login),
                    onPressed: () => Navigator.pushNamed(context, Profile.id),
                  ),
                  Padding(padding: const EdgeInsets.only(bottom: 20.0)),
                  Text(
                    'If u dont have acc',
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
